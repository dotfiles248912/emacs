#+title: Emacs config
#+author: Ket Rivers, robertdq, Dascalu Robert
#+description: GNU Emacs config, modified after Derek Taylor's personal GNU Emacs config.
#+startup: showeverything

* init.el
To config Emacs trough a org file, the following code needs to be put in init.el. This tells init.el to use the source code blocks from this file (config.org).  
All the settings and configs will be written in this file. init.el will read all the source code blocks from here.

#+begin_example
(org-babel-load-file
 (expand-file-name
  "config.org"
  user-emacs-directory))
#+end_example

* Programs to load first
The order in which the various Emacs modules load is very important. So the very first code block is going to contain essential modules that many other modules will depend on later in this config.

** Setup package.el to work with MELPA
#+begin_src emacs-lisp
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-refresh-contents)
(package-initialize)
#+end_src

** use-package
Install use-package and enable ':ensure t' globally. The ':ensure' keyword causes the package(s) within use-package statements to be installed automatically if not already present on your system. To avoid having to add ':ensure t' to every use-package statement in this config, I set 'use-package-always-ensure'.

#+begin_src emacs-lisp
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(setq use-package-always-ensure t)
#+end_src

** evil-mode
Evil is an extensible 'vi' layer for Emacs. It emulates the main features of Vim, and provides facilities for writing custom extensions. Evil Collection is also installed since it adds 'evil' bindings to parts of Emacs that the standard Evil package does not cover, such as: calendar, help-mode and ibuffer.
  
#+begin_src emacs-lisp
(use-package evil
  :init ; Tweak evil's configuration before loading it
  (setq evil-want-integration t) ; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (evil-mode))
(use-package evil-collection
  :after evil
  :config
  (setq evil-collection-mode-list '(dired ibuffer))
  ;;(setq evil-collection-mode-list '(dashboard dired ibuffer))
  (evil-collection-init))
(use-package evil-tutor)
#+end_src

** General keybindings
general.el allows us to set keybindings. This will help using <SPC> as the prefix key. General makes setting keybindings (especially with SPC) much easier.  All of the keybindings we set later in the config depend on general being loaded.

#+begin_src emacs-lisp
(use-package general
  :config
  (general-evil-setup t))
#+end_src

* All the icons
This is an icon set that can be used with dashboard, dired, ibuffer and other Emacs programs.
  
#+begin_src emacs-lisp
(use-package all-the-icons)
#+end_src

* Fonts

** Setting the font face
#+begin_src emacs-lisp
(set-face-attribute 'default nil
  :font "Mononoki Nerd Font"
  :height 100
  :weight 'medium)
(set-face-attribute 'variable-pitch nil
  :font "Ubuntu Nerd Font"
  :height 100
  :weight 'medium)
(set-face-attribute 'fixed-pitch nil
  :font "Mononoki Nerd Font"
  :height 100
  :weight 'medium)
;; Makes commented text and keywords italics.
;; This is working in emacsclient but not emacs.
;; Your font must have an italic face available.
;;(set-face-attribute 'font-lock-comment-face nil
;;  :slant 'italic)
;;(set-face-attribute 'font-lock-keyword-face nil
;;  :slant 'italic)

;; Uncomment the following line if line spacing needs adjusting.
;;(setq-default line-spacing 0.12)

;; Needed if using emacsclient. Otherwise, your fonts will be smaller than expected.
;;(add-to-list 'default-frame-alist '(font . "SauceCodePro NF"UbuntuMono Nerd Font 10"))

;; Changes certain keywords to symbols, such as lamda!
(setq global-prettify-symbols-mode t)
#+end_src

** Zooming in and out
You can use the bindings CTRL plus =/- for zooming in/out.  You can also use CTRL plus the mouse wheel for zooming in/out.

#+begin_src emacs-lisp
;; zoom in/out like we do everywhere else.
(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)
#+end_src

* General keybindings
General.el allows us to set keybindings. As a longtime Doom Emacs user, I have grown accustomed to using SPC as the prefix key. It certainly is easier on the hands than constantly using CTRL for a prefix.

#+begin_src emacs-lisp
(nvmap :keymaps 'override :prefix "SPC"
       "SPC"   '(counsel-M-x :which-key "M-x")
       "c c"   '(compile :which-key "Compile")
       "c C"   '(recompile :which-key "Recompile")
       "h r r" '((lambda () (interactive) (load-file "~/.emacs.d/init.el")) :which-key "Reload emacs config")
       "t t"   '(toggle-truncate-lines :which-key "Toggle truncate lines"))
(nvmap :keymaps 'override :prefix "SPC"
       "m *"   '(org-ctrl-c-star :which-key "Org-ctrl-c-star")
       "m +"   '(org-ctrl-c-minus :which-key "Org-ctrl-c-minus")
       "m ."   '(counsel-org-goto :which-key "Counsel org goto")
       "m e"   '(org-export-dispatch :which-key "Org export dispatch")
       "m f"   '(org-footnote-new :which-key "Org footnote new")
       "m h"   '(org-toggle-heading :which-key "Org toggle heading")
       "m i"   '(org-toggle-item :which-key "Org toggle item")
       "m n"   '(org-store-link :which-key "Org store link")
       "m o"   '(org-set-property :which-key "Org set property")
       "m t"   '(org-todo :which-key "Org todo")
       "m x"   '(org-toggle-checkbox :which-key "Org toggle checkbox")
       "m B"   '(org-babel-tangle :which-key "Org babel tangle")
       "m I"   '(org-toggle-inline-images :which-key "Org toggle inline imager")
       "m T"   '(org-todo-list :which-key "Org todo list")
       "o a"   '(org-agenda :which-key "Org agenda")
       )
#+end_src
  
* GUI tweaks
Let's make GNU Emacs look a little better.

** Disable Menubar, Toolbars and Scrollbars
#+begin_src emacs-lisp
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-startup-screen t)
#+end_src

** Display Line Numbers and Truncated Lines
#+begin_src emacs-lisp
(global-display-line-numbers-mode 1)
(global-visual-line-mode t)
#+end_src

** Change Modeline To Doom's Modeline
#+begin_src emacs-lisp
  (use-package doom-modeline
  :ensure t)
  (doom-modeline-mode 1)
#+end_src

* Programming language support
Adding packages for programming langauges, so we can have nice things like syntax highlighting.

#+begin_src emacs-lisp
(use-package haskell-mode)
(use-package lua-mode)
(use-package markdown-mode)
(use-package julia-mode)
(use-package ess)
(use-package ob-ess-julia)
#+end_src

 
* ORG mode
Org Mode is =THE= killer feature within Emacs.  But it does need some tweaking.

** Source Code Block Tag Expansion
Org-tempo is a package that allows for '<s' followed by TAB to expand to a begin_src tag.  Other expansions available include:

| Typing the below + TAB | Expands to ...                          |
|------------------------+-----------------------------------------|
| <a                     | '#+BEGIN_EXPORT ascii' … '#+END_EXPORT  |
| <c                     | '#+BEGIN_CENTER' … '#+END_CENTER'       |
| <C                     | '#+BEGIN_COMMENT' … '#+END_COMMENT'     |
| <e                     | '#+BEGIN_EXAMPLE' … '#+END_EXAMPLE'     |
| <E                     | '#+BEGIN_EXPORT' … '#+END_EXPORT'       |
| <h                     | '#+BEGIN_EXPORT html' … '#+END_EXPORT'  |
| <l                     | '#+BEGIN_EXPORT latex' … '#+END_EXPORT' |
| <q                     | '#+BEGIN_QUOTE' … '#+END_QUOTE'         |
| <s                     | '#+BEGIN_SRC' … '#+END_SRC'             |
| <v                     | '#+BEGIN_VERSE' … '#+END_VERSE'         |

#+begin_src emacs-lisp
(use-package org-tempo
  :ensure nil) ;; tell use-package not to try to install org-tempo since it's already there.
#+end_src

** Source Code Block Syntax Highlighting
We want the same syntax highlighting in source blocks as in the native language files.

#+begin_src emacs-lisp
(setq org-src-fontify-natively t
    org-src-tab-acts-natively t
    org-confirm-babel-evaluate nil
    org-edit-src-content-indentation 0)
#+end_src

** Automatically Create Table of Contents
Toc-org helps you to have an up-to-date table of contents in org files without exporting (useful useful for README files on GitHub).  Use :TOC: to create the table.

#+begin_src emacs-lisp
  (use-package toc-org
    :commands toc-org-enable
    :init (add-hook 'org-mode-hook 'toc-org-enable))
#+end_src

** Make M-RET Not Add Blank Lines
#+begin_src emacs-lisp
(setq org-blank-before-new-entry (quote ((heading . nil)
                                         (plain-list-item . nil))))
#+end_src

* Scrolling
Emacs default scrolling is annoying because of the sudden half-page jumps. Also, I wanted to adjust the scrolling speed.

#+begin_src emacs-lisp
(setq scroll-conservatively 101) ;; value greater than 100 gets rid of half page jumping
(setq mouse-wheel-scroll-amount '(3 ((shift) . 3))) ;; how many lines at a time
(setq mouse-wheel-progressive-speed t) ;; accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
#+end_src

* Splits and window controlls
#+begin_src emacs-lisp
(winner-mode 1)
(nvmap :prefix "SPC"
       ;; Window splits
       "w c"   '(evil-window-delete :which-key "Close window")
       "w n"   '(evil-window-new :which-key "New window")
       "w s"   '(evil-window-split :which-key "Horizontal split window")
       "w v"   '(evil-window-vsplit :which-key "Vertical split window")
       ;; Window motions
       "w h"   '(evil-window-left :which-key "Window left")
       "w j"   '(evil-window-down :which-key "Window down")
       "w k"   '(evil-window-up :which-key "Window up")
       "w l"   '(evil-window-right :which-key "Window right")
       "w w"   '(evil-window-next :which-key "Goto next window")
       ;; winner mode
       "w <left>"  '(winner-undo :which-key "Winner undo")
       "w <right>" '(winner-redo :which-key "Winner redo"))
#+end_src

* Theme
We need a nice colorscheme. The Doom Emacs guys have a nice collection of themes, so let's install them!

#+begin_src emacs-lisp
(use-package doom-themes)
(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
      doom-themes-enable-italic t) ; if nil, italics is universally disabled
(load-theme 'doom-one t)
#+end_src

* Which key
Which-key is a minor mode for Emacs that displays the key bindings following your currently entered incomplete command (a prefix) in a popup.

=NOTE:= Which-key has an annoying bug that in some fonts and font sizes, the bottom row in which key gets covered up by the modeline.

#+begin_src emacs-lisp
(use-package which-key
  :init
  (setq which-key-side-window-location 'bottom
        which-key-sort-order #'which-key-key-order-alpha
        which-key-sort-uppercase-first nil
        which-key-add-column-padding 1
        which-key-max-display-columns nil
        which-key-min-display-lines 6
        which-key-side-window-slot -10
        which-key-side-window-max-height 0.25
        which-key-idle-delay 0.8
        which-key-max-description-length 25
        which-key-allow-imprecise-window-fit t
        which-key-separator " → " ))
(which-key-mode)
#+end_src

* Runtime performance
Dial the GC threshold back down so that garbage collection happens more frequently but in less time.
#+begin_src emacs-lisp
;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))
#+end_src
